use std::io;

pub mod client;
pub mod frame;
pub mod server;
pub mod stream;

#[derive(Debug)]
pub enum Error {
    IoError(io::Error),
    InsufficientData,
    InvalidOpcode,
    IncorrectFormat,
    ConnectionClosed,
    InvalidUTF8,
    InvalidRequest(String),
    HttpRequest(String),
}

#[derive(Copy, Clone)]
pub enum CloseReason {
    Normal = 1000,
    ProtocolError = 1002,
    UnexpectedData = 1007,
}
