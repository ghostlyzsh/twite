use std::{io::{Write, self, Read, ErrorKind, Cursor}, net::TcpStream, str, collections::HashMap};
use rand::Rng;
use base64::{Engine as _, engine::general_purpose};
use sha1::{Sha1, Digest};
use url::Url;

use crate::{Error, stream::WsStream};

pub fn connect(url: Url) -> Result<WsStream, Error> {
    let mut rng = rand::thread_rng();
    let host = format!("{}:{}", url.host_str().unwrap(), url.port().unwrap());
    let mut stream = match TcpStream::connect(host.clone()) {
        Ok(s) => s,
        Err(e) => return Err(Error::IoError(e))
    };
    let raw_key: [u8; 16] = rng.gen();
    let key = general_purpose::STANDARD.encode(raw_key);

    let message = if let Some(query) = url.query() {
        format!(
           "GET {} HTTP/1.1\r
Upgrade: websocket\r
Host: {}\r
Connection: Upgrade\r
Sec-WebSocket-Key: {}\r
Sec-WebSocket-Version: 13\r\n\r\n",
            format!("{}?{}", url.path(), query), host, key)
    } else {
        format!(
           "GET {} HTTP/1.1\r
Upgrade: websocket\r
Host: {}\r
Connection: Upgrade\r
Sec-WebSocket-Key: {}\r
Sec-WebSocket-Version: 13\r\n\r\n",
            url.path(), host, key)
    };
    //TODO: extensions and protocol

    match stream.write_all(message.as_bytes()) {
        Ok(_) => {}
        Err(e) => return Err(Error::IoError(e))
    };
    let mut buf = Vec::new();
    let mut buf_bytes = [0u8; 1024];
    let mut first_frame_index: Option<usize> = None;
    loop {
        let read = match stream.read(&mut buf_bytes) {
            Ok(s) => s,
            Err(e) => return Err(Error::IoError(e))
        };

        if read == 0 {
            break;
        }

        buf.extend_from_slice(&buf_bytes[0..read]);

        if buf.ends_with(&[0x0d, 0x0a, 0x0d, 0x0a]) {
            break;
        }

        let mut should_break = false;
        for i in 0..buf.len() {
            let slice = &buf[0..i+1];
            if slice.ends_with(&[0x0d, 0x0a, 0x0d, 0x0a]) {
                first_frame_index = Some(i+1);
                should_break = true;
                break;
            }
        }
        if should_break { break; }
    }
    
    let message: &str = match str::from_utf8(if let Some(index) = first_frame_index {
        &buf[0..index]
    } else {
        buf.as_slice()
    }) {
        Ok(v) => v,
        Err(e) => return Err(Error::IoError(io::Error::new(ErrorKind::Other, format!("Invalid UTF-8 string: {}", e)))),
    };
    let message: Vec<&str> = message.split("\n").collect();
    let mut fields: HashMap<String, String> = HashMap::new();
    message.iter().skip(1).for_each(|s| {
        if s.trim().is_empty() { return; }
        let values: Vec<&str> = s.split(": ").collect();
        let name = values[0].trim().to_lowercase();
        let value = if name != "sec-websocket-accept" {
            values[1].trim().to_lowercase()
        } else {
            values[1].trim().to_string()
        };
        fields.insert(name, value);
    });
    assert_eq!(message[0].trim(), "HTTP/1.1 101 Switching Protocols");
    assert_eq!(fields.get("upgrade").ok_or(Error::InvalidRequest("No Upgrade found".to_string()))?.to_lowercase(), "websocket");
    assert_eq!(fields.get("connection").ok_or(Error::InvalidRequest("No Connection found".to_string()))?.to_lowercase(), "upgrade");
    let accept = fields.get("sec-websocket-accept").ok_or(Error::InvalidRequest("No Accept key found".to_string()))?;
    let gen_accept = [key, "258EAFA5-E914-47DA-95CA-C5AB0DC85B11".to_string()]
        .concat();
    let mut hasher = Sha1::new();
    hasher.update(gen_accept);
    let gen_accept = general_purpose::STANDARD.encode(hasher.finalize().as_slice());
    assert_eq!(accept.as_bytes(), gen_accept.as_bytes());
    let mut extra = Cursor::new(Vec::new());
    if let Some(index) = first_frame_index {
        match extra.write_all(&buf[index..]) {
            Ok(_) => {}
            Err(e) => return Err(Error::IoError(e))
        };
        extra.set_position(0);
    }
    //TODO: extensions and protocol
    Ok(WsStream {
        url,
        key: Some(raw_key),
        write: stream.try_clone().unwrap(),
        read: extra.chain(stream),
        closing: false,
        fragment: None,
        client: true,
    })
}
